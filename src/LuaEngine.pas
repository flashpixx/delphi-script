(*
 * ######################################################################################
 * # LGPL License                                                                       #
 * #                                                                                    #
 * # This file is part of the Delphi-Script                                             #
 * # Copyright (c) 2019, Delphi-Script (philipp.kraus@flashpixx.de)                     #
 * # This program is free software: you can redistribute it and/or modify               #
 * # it under the terms of the GNU Lesser General Public License as                     #
 * # published by the Free Software Foundation, either version 3 of the                 #
 * # License, or (at your option) any later version.                                    #
 * #                                                                                    #
 * # This program is distributed in the hope that it will be useful,                    #
 * # but WITHOUT ANY WARRANTY; without even the implied warranty of                     #
 * # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                      #
 * # GNU Lesser General Public License for more details.                                #
 * #                                                                                    #
 * # You should have received a copy of the GNU Lesser General Public License           #
 * # along with this program. If not, see http://www.gnu.org/licenses/                  #
 * ######################################################################################
 *)

unit LuaEngine;

interface

uses sysutils, strutils, variants;

type

    // exceptions
    ELuaRuntimeError = class(EFormatError);
    ELuaTypeError = class(EArgumentException);


    (*
     * pascal function for calling from lua, must be a c-function (cdecl)
     * @param p_lua pointer to lua engine
     * @return number of arguments, which must be pushed to the stack first
     *)
    TLuaFunction = function( p_lua : pointer ) : integer; cdecl;


    (*
     * any kind of argument
     *)
    Argument = class( TInterfacedObject )

        private

            _values : Array of variant;

        public

            constructor Create(); overload;

            constructor Create( const p_values : Array of variant ); overload;

            function IsEmpty() : boolean;

            function GetSize() : longint;

            function GetValue( const p_index : longint ) :  variant;

            function AsString( const p_index : longint ) : string;

            function AsInteger( const p_index : longint ) : integer;

            function AsDouble( const p_index : longint ) : double;

            function AsBoolean( const p_index : longint ) : boolean;

            function IsBoolean( const p_index : longint ) : boolean;

            function IsDouble( const p_index : longint ) : boolean;

            function IsInteger( const p_index : longint ) : boolean;

            function IsString( const p_index : longint ) : boolean;

            function IsNull( const p_index : longint ) : boolean;

    end;


    (*
     * lua script object
     *)
    Lua = class( TInterfacedObject )


        public

            (*
             * ctor
             * @param p_defaultlibraries use default lua libraries
             *)
            constructor Create( p_defaultlibraries : boolean = true ); overload;

            (*
             * ctor
             * @param p_script lua script
             * @param p_defaultlibraries use default lua libraries
             *)
            constructor Create( const p_script : string; p_defaultlibraries : boolean = true ); overload;

            (*
             * dtor
             *)
            destructor Destroy(); override;

            function parse( const p_script : string ) : Lua;

            function Execute( const p_name : string ) : Argument; overload;

            function Execute( const p_name : string; const p_args : Array of variant ) : Argument; overload;

            function IsExecutable( const p_name: string ) : boolean;

            function GetVersion() : string;

            function SetGlobal( const p_name : string; const p_value : variant ) : Lua;

            function GetGlobal( const p_name : string ) : Argument;

            function PushError( const p_message : string ) : Lua; overload;

            (*
             * sets the lua import funciton, which is called on require / import
             * @param p_function function
             * @return self-reference
             *)
            function ImportFunction( const p_function : TLuaFunction ) : Lua;

            (*
             * adds a function to the lua script with an unique name
             * @param p_name lua name function
             * @param p_function function
             * @return self-reference
             *)
            function AddFunction( const p_name : string; const p_function : TLuaFunction ) : Lua;

            (*
             * pushs a value to the stack
             * @param p_lua lua engine pointer
             * @param p_value value
             *)
            class procedure Push( p_lua : pointer; const p_value : variant ); static;

            (*
             * clears the stack
             * @param p_lua lua engine pointer
             *)
            class procedure ClearStack( p_lua : pointer ); static;

            (*
             * returns the current stack as return structure
             * @param p_lua lua engine pointer
             * @return return element
             *)
            class function StackToArgument( p_lua : pointer ) : Argument; static;

            (*
             * push a error message on the stack
             * @param p_lua lua engine
             * @param p_message error message
             *)
            class procedure PushError( p_lua : pointer; const p_message : string ); static; overload;

        private

            (*
             * lua engine pointer
             *)
            _engine : pointer;

            (*
             * throws an exception with the message of the lua stack
             * @param p_return return value
             *)
            procedure ThrowException( const p_return : integer );

    end;

implementation

    uses ULua;


    constructor Argument.Create(); overload;
    begin
        setlength( _values, 0 );
    end;

    constructor Argument.Create( const p_values : Array of variant ); overload;
    var
        i : longint;
        e : variant;
    begin
        setlength( _values, length( p_values ) );
        i := 0;
        for e in p_values do
        begin
            _values[i] := e;
            inc(i);
        end;
    end;

    function Argument.IsEmpty() : boolean;
    begin
        result := length( _values ) = 0;
    end;

    function Argument.GetSize() : longint;
    begin
        result := length( _values )
    end;

    function Argument.GetValue( const p_index : integer ) : variant;
    begin
        result := _values[p_index];
    end;

    function Argument.AsString( const p_index : integer ) : string;
    begin
        result := _values[p_index];
    end;

    function Argument.AsInteger( const p_index : integer ) : integer;
    begin
        result := integer( _values[p_index] );
    end;

    function Argument.AsDouble( const p_index : integer ) : double;
    begin
        result := double( _values[p_index] );
    end;

    function Argument.AsBoolean( const p_index : integer ) : boolean;
    begin
        result := boolean( _values[p_index] );
    end;

    function Argument.IsBoolean( const p_index : longint ) : boolean;
    begin
        result := varisbool( _values[p_index] );
    end;

    function Argument.IsDouble( const p_index : longint ) : boolean;
    begin
        result := varisfloat( _values[p_index] );
    end;

    function Argument.IsInteger( const p_index : longint ) : boolean;
    begin
        result := varisordinal( _values[p_index] );
    end;

    function Argument.IsString( const p_index : longint ) : boolean;
    begin
        result := varisstr( _values[p_index] );
    end;

    function Argument.IsNull( const p_index : longint ) : boolean;
    begin
        result := varisnull( _values[p_index] ) or  varisclear( _values[p_index] ) or varisempty( _values[p_index] );
    end;


    constructor Lua.Create( p_defaultlibraries : boolean = true ); overload;
    begin
        _engine  := luaL_newstate();
        if p_defaultlibraries then
            luaL_openlibs( _engine  );
    end;

    constructor Lua.Create( const p_script : string; p_defaultlibraries : boolean = true ); overload;
    begin
        _engine  := luaL_newstate();
        if p_defaultlibraries then
            luaL_openlibs( _engine  );

        ThrowException( luaL_loadstring( _engine, pchar( p_script ) ) );
        ThrowException( lua_pcall( _engine, 0, LUA_MULTRET, 0 ) );
    end;

    destructor Lua.Destroy();
    begin
        lua_close( _engine  );
        inherited;
    end;

    function Lua.parse( const p_script : string ) : Lua;
    begin
        ThrowException( luaL_loadstring( _engine, pchar( p_script ) ) );
        ThrowException( lua_pcall( _engine, 0, LUA_MULTRET, 0 ) );
        result := self;
    end;

    function Lua.ImportFunction( const p_function : TLuaFunction ) : Lua;
    begin
        // http://lua-users.org/lists/lua-l/2012-01/msg00124.html
        // https://gist.github.com/randrews/1131236

        luaL_loadstring( _engine, 'table.insert(package.loaders, 3, ...)' );
        lua_pushcfunction( _engine, @p_function );
        lua_call( _engine, 1, 0 );
        result := self;
    end;

    function Lua.AddFunction( const p_name : string; const p_function : TLuaFunction ) : Lua;
    begin
        lua_register( _engine, pchar( p_name ), @p_function );
        result := self;
    end;

    function Lua.Execute( const p_name : string ) : Argument; overload;
    begin
        result := Execute( p_name, [] );
    end;

    function Lua.Execute( const p_name : string; const p_args : Array of variant ) : Argument; overload;
    var
        i : variant;
    begin
        lua_getglobal( _engine, pchar( p_name ) );

        for i in p_args do
        begin
            Push( _engine, i );
        end;

        ThrowException( lua_pcall( _engine, lua_gettop( _engine ) - 1, LUA_MULTRET, 0 ) );

        result := StackToArgument( _engine );
        ClearStack( _engine );
    end;

    function Lua.GetVersion() : string;
    begin
        lua_getglobal( _engine, pchar( '_VERSION' ) );
        result := replacetext( strpas( lua_tostring( _engine, -1 ) ), 'Lua', '' );
        ClearStack( _engine );
    end;

    function lua.SetGlobal( const p_name : string; const p_value : variant ) : Lua;
    begin
        Push( _engine, p_value );
        lua_setglobal( _engine, pchar( p_name ) );
        result := self;
    end;

    function Lua.GetGlobal( const p_name : string ) : Argument;
    begin
        lua_getglobal( _engine, pchar( p_name ) );
        result := StackToArgument( _engine );
        ClearStack( _engine );
    end;

    function Lua.PushError( const p_message : string ) : Lua; overload;
    begin
        PushError( _engine, p_message );
        result := self;
    end;

    procedure Lua.ThrowException( const p_return : integer );
    var
        l_error : string;
    begin
        if p_return = LUA_OK then
            exit;

        l_error := strpas( lua_tostring( _engine, -1 ) );
        ClearStack( _engine );

        raise ELuaRuntimeError.Create( l_error );
    end;

    class procedure Lua.Push( p_lua : pointer; const p_value : variant ); static;
    begin
        case vartype( p_value ) of

            varempty:
                lua_pushnil( p_lua );
            varnull:
                lua_pushnil( p_lua );

            varSingle:
                lua_pushinteger( p_lua, integer( p_value ) );
            varShortInt:
                lua_pushinteger( p_lua, integer( p_value ) );
            varSmallint:
                lua_pushinteger( p_lua, integer( p_value ) );
            varInteger:
                lua_pushinteger( p_lua, integer( p_value ) );
            varByte:
                lua_pushinteger( p_lua, integer( p_value ) );
            varWord:
                lua_pushinteger( p_lua, integer( p_value ) );
            varQWord:
                lua_pushinteger( p_lua, integer( p_value ) );

            varDouble:
                lua_pushnumber( p_lua, double( p_value ) );
            varInt64:
                lua_pushnumber( p_lua, double( p_value ) );
            varLongWord:
                lua_pushnumber( p_lua, double( p_value ) );

            varString:
                lua_pushstring( p_lua, pchar( vartostr( p_value ) ) );
            varOleStr:
                lua_pushstring( p_lua, pchar( vartostr( p_value ) ) );
            varStrArg:
                lua_pushstring( p_lua, pchar( vartostr( p_value ) ) );

            varBoolean:
                lua_pushboolean( p_lua, integer( p_value ) );

        else
            ClearStack( p_lua );
            raise ELuaTypeError.Create( format('argument [%s] type cannot be used as lua type', [p_value] ) );
        end;
    end;

    class procedure Lua.ClearStack( p_lua : pointer  ); static;
    begin
        if lua_gettop( p_lua ) > 0 then
            lua_pop( p_lua, lua_gettop( p_lua ) );
    end;

    function Lua.IsExecutable( const p_name: string ) : boolean;
    begin
        lua_getglobal( _engine, pchar( p_name ) );
        result := boolean( lua_isfunction( _engine, -1 ) );
        lua_remove( _engine, -1 );
    end;

    class function Lua.StackToArgument( p_lua : pointer ) : Argument; static;
    var
        i : integer;
        l_return : array of variant;
    begin
        if lua_gettop( p_lua ) = 0 then
        begin
            result := Argument.Create();
            exit;
        end;


        setlength( l_return, lua_gettop( p_lua ) );

        for i :=  1 to lua_gettop( p_lua ) do
        begin

            if lua_isnoneornil( p_lua, i ) then
            begin
                l_return[ i - 1 ] := null;
                continue;
            end;

            if lua_isinteger( p_lua, i ) <> 0 then
            begin
                l_return[ i - 1 ] := lua_tointeger( p_lua, i );
                continue;
            end;

            if lua_isnumber( p_lua, i ) <> 0 then
            begin
                l_return[ i - 1 ] := lua_tonumber( p_lua, i );
                continue;
            end;

            if lua_isboolean( p_lua, i ) then
            begin
                l_return[ i - 1 ] := boolean( lua_toboolean( p_lua, i ) );
                continue;
            end;

            // string must be the last check because every datatype can be converted to a string
            if lua_isstring( p_lua, i ) <> 0 then
            begin
                l_return[ i - 1 ] := strpas( lua_tostring( p_lua, i ) );
                continue;
            end;

            raise ELuaTypeError.Create( format( 'lua type [%s] on index [%i] cannot be converted to pascal type', [strpas( lua_typename( p_lua, i ) ), i] ) )

        end;

        ClearStack( p_lua );

        result := Argument.Create( l_return );
    end;

    class procedure Lua.PushError( p_lua : pointer; const p_message : string ); static; overload;
    begin
        lua_pushfstring( p_lua, pchar( p_message ) );
    end;

end.
