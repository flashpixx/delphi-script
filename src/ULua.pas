(*
 * ######################################################################################
 * # LGPL License                                                                       #
 * #                                                                                    #
 * # This file is part of the Delphi-Script                                             #
 * # Copyright (c) 2019, Delphi-Script (philipp.kraus@flashpixx.de)                     #
 * # This program is free software: you can redistribute it and/or modify               #
 * # it under the terms of the GNU Lesser General Public License as                     #
 * # published by the Free Software Foundation, either version 3 of the                 #
 * # License, or (at your option) any later version.                                    #
 * #                                                                                    #
 * # This program is distributed in the hope that it will be useful,                    #
 * # but WITHOUT ANY WARRANTY; without even the implied warranty of                     #
 * # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                      #
 * # GNU Lesser General Public License for more details.                                #
 * #                                                                                    #
 * # You should have received a copy of the GNU Lesser General Public License           #
 * # along with this program. If not, see http://www.gnu.org/licenses/                  #
 * ######################################################################################
 *)

unit ULua;

(*
 * unit to encapsulate c library function
 * @see https://www.lua.org
 * @see https://www.lua.org/pil/24.html
 *)
interface

    uses ctypes;

    const
        LUA_MULTRET	: integer = -1;

        LUA_OK : integer = 0;
        LUA_YIELD : integer = 1;
        LUA_ERRRUN : integer = 2;
        LUA_ERRSYNTAX : integer = 3;
        LUA_ERRMEM : integer =  4;
        LUA_ERRGCMM : integer =  5;
        LUA_ERRERR : integer = 6;

        LUA_TNONE : integer = -1;
        LUA_TNIL : integer = 0;
        LUA_TBOOLEAN : integer = 1;
        LUA_TLIGHTUSERDATA : integer = 2;
        LUA_TNUMBER : integer = 3;
        LUA_TSTRING : integer = 4;
        LUA_TTABLE : integer = 5;
        LUA_TFUNCTION : integer = 6;
        LUA_TUSERDATA : integer = 7;
        LUA_TTHREAD : integer = 8;
        LUA_NUMTAGS : integer = 9;

    (*
     * create a lua interpreter
     * @return lua engine pointer
     *)
    function luaL_newstate() : pointer; cdecl; external;

    (*
     * load default lua libraries
     * @param p_lua engine pointer
     *)
    procedure luaL_openlibs( p_lua : pointer ); cdecl; external;

    (*
     * releases the engine
     * @param p_lua engine pointer
     *)
    procedure lua_close( p_lua : pointer ); cdecl; external;


    (*
     * loads a lua script as string
     * @param p_lua lua engine pointer
     * @param p_script script as string
     * @return parser result
     *)
    function luaL_loadstring( p_lua : pointer; p_script : pchar ) : integer; cdecl; external;

    (*
     * rotates the lua result stack
     * @param p_lua lua engine pointer
     * @param p_index item index
     * @param p_pos rotate index
     *)
    procedure lua_rotate( p_lua : pointer; p_index : integer; p_pos : integer ); cdecl; external;

    (*
     * set element to stack top
     * @param p_lua lua engine pointer
     * @param p_index item index
     *)
    procedure lua_settop( p_lua : pointer; p_index : integer ); cdecl; external;

    (*
     * returns string of the lua stack
     * @param p_lua lua engine pointer
     * @param p_index item index
     * @param p_size string size
     * @return string
     *)
    function lua_tolstring( p_lua : pointer; p_index : integer; p_size : integer ) : pchar; cdecl; external;

    (*
     * returns number of the lua stack
     * @param p_lua lua engine pointer
     * @param p_index item index
     * @param p_size size
     * @return double
     *)
    function lua_tonumberx( p_lua : pointer; p_index : integer; p_size : pointer ) : double; cdecl; external;

    (*
     * returns integer of the lua stack
     * @param p_lua lua engine pointer
     * @param p_index item index
     * @param p_size size
     * @return integer
     *)
    function lua_tointegerx( p_lua : pointer; p_index : integer; p_size : pointer ) : integer; cdecl; external;

    (*
     * returns boolean of the lua stack
     * @param p_lua lua engine pointer
     * @param p_index item index
     * @return boolean as integer
     *)
    function lua_toboolean( p_lua : pointer; p_index : integer ) : integer; cdecl; external;

    (*
     * calls the current stack
     * @param p_lua lua engine pointer
     * @param p_args number of arguments
     * @param p_results number of results
     * @param p_errorfunction error function index
     * @param p_kcontext function context
     * @param p_kfunction function pointer
     * @return execution result
     *)
    function lua_pcallk( p_lua : pointer; p_args : integer; p_results : integer; p_errorfunction : integer; p_kcontext : pointer; p_kfunction : pointer ) : integer; cdecl; external;

    (*
     * calls the current stack
     * @param p_lua lua engine pointer
     * @param p_args number of arguments
     * @param p_results number of results
     * @param p_kcontext function context
     * @param p_kfunction function pointer
     *)
    procedure lua_callk( p_lua : pointer; p_args : integer; p_results : integer; p_kcontext : pointer; p_kfunction : pointer ); cdecl; external;

    (*
     * gets a global element and push it on the stack
     * @param p_lua lua engine pointer
     * @param p_name global name
     * @return execution result
     *)
    function lua_getglobal( p_lua : pointer; p_name : pchar ) : integer; cdecl; external;

    (*
     * returns the top size of the stack
     * @param p_lua lua engine pointer
     * @return element number
     *)
    function lua_gettop( p_lua : pointer ) : integer; cdecl; external;

    (*
     * pushs a nil value to the stack
     * @param p_lua lua engine pointer
     *)
    procedure lua_pushnil( p_lua : pointer ); cdecl; external;

    (*
     * pushs a double number value to the stack
     * @param p_lua lua engine pointer
     * @param p_number number
     *)
    procedure lua_pushnumber( p_lua : pointer; p_number : double ); cdecl; external;

    (*
     * pushs a integer number value to the stack
     * @param p_lua lua engine pointer
     * @param p_number number
     *)
    procedure lua_pushinteger( p_lua : pointer; p_number : integer ); cdecl; external;

    (*
     * pushs a boolean number value to the stack
     * @param p_lua lua engine pointer
     * @param p_number boolean as int value
     *)
    procedure lua_pushboolean( p_lua : pointer; p_number : integer ); cdecl; external;

    (*
     * pushs a pointer number value to the stack
     * @param p_lua lua engine pointer
     * @param p_ptr pointer
     *)
    procedure lua_pushlightuserdata( p_lua : pointer; p_ptr : pointer ); cdecl; external;

    (*
     * pushs a string number value to the stack
     * @param p_lua lua engine pointer
     * @param p_string string value
     * @return input string
     *)
    function lua_pushstring( p_lua : pointer; const p_string : pchar ) : pchar; cdecl; external;

    (*
     * push a error message to the stack
     * @param p_lua lua engine pointer
     * @param p_message error message
     * @return string
     *)
    function lua_pushfstring( p_lua : pointer;  p_message : pchar ) : pchar; cdecl; varargs; external;

    (*
     * check if the element is a number
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean as integer of check result
     *)
    function lua_isnumber( p_lua : pointer; p_index : integer ) : integer; cdecl; external;

    (*
     * check if the element is a string
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean as integer of check result
     *)
    function lua_isstring( p_lua : pointer; p_index : integer ) : integer; cdecl; external;

    (*
     * check if the element is a number
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean as integer of check result
     *)
    function lua_isinteger( p_lua : pointer; p_index : integer ) : integer; cdecl; external;

    (*
     * check if the element is user-data
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean as integer of check result
     *)
    function lua_isuserdata( p_lua : pointer; p_index : integer ) : integer; cdecl; external;

    (*
     * returns the lua type of an element
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return lua type index
     *)
    function lua_type( p_lua : pointer; p_index : integer ) : integer; cdecl; external;

    (*
     * returns the lua type of an element
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return lua type string
     *)
    function lua_typename( p_lua : pointer; p_index : integer ) : pchar; cdecl; external;

    (*
     * pushs a closure to the stack
     * @param p_lua lua engine pointer
     * @param p_ptr closure pointer
     * @param p_value value number
     *)
    procedure lua_pushcclosure( p_lua : pointer; p_ptr : pointer; p_value : integer ); cdecl; external;

    (*
     * sets a global name
     * @param p_lua lua engine pointer
     * @param p_name name
     *)
    procedure lua_setglobal( p_lua : pointer; p_name : pchar ); cdecl; external;

    (*
     * pop elements from the stack
     * @param p_lua lua engine pointer
     * @param p_index item size
     *)
    procedure lua_pop( p_lua : pointer; p_index : integer );

    (*
     * removes elements from the stack
     * @param p_lua lua engine pointer
     * @param p_index index position
     *)
    procedure lua_remove( p_lua : pointer; p_index : integer );

    (*
     * returns a string from the stack element
     * @param p_lua lua engine pointer
     * @param p_index index position
     * @return result string
     *)
    function lua_tostring( p_lua : pointer; p_index : integer ) : pchar;

    (*
     * returns a double from the stack element
     * @param p_lua lua engine pointer
     * @param p_index index position
     * @return result string
     *)
    function lua_tonumber( p_lua : pointer; p_index : integer ) : double;

    (*
     * returns a integer from the stack element
     * @param p_lua lua engine pointer
     * @param p_index index position
     * @return result string
     *)
    function lua_tointeger( p_lua : pointer; p_index : integer ) : integer;

    (*
     * executes the stack
     * @param p_lua lua engine pointer
     * @param p_args number of arguments
     * @param p_results number of results
     * @param p_errorfunction error function index
     * @return execution result
     *)
     function lua_pcall( p_lua : pointer; p_args : integer; p_results : integer; p_errorfunction : integer ) : integer;

     (*
      * executes the stack
      * @param p_lua lua engine pointer
      * @param p_args number of arguments
      * @param p_results number of results
      *)
     procedure lua_call( p_lua : pointer; p_args : integer; p_results : integer );

    (*
     * check if the element is a function
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_isfunction( p_lua : pointer; p_index : integer ) : boolean;

    (*
     * check if the element is a table
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_istable( p_lua : pointer; p_index : integer ) : boolean;

    (*
     * check if the element is a light-user-data
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_islightuserdata( p_lua : pointer; p_index : integer ) : boolean;

    (*
     * check if the element is a nil
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_isnil( p_lua : pointer; p_index : integer ) : boolean;

    (*
     * check if the element is a boolean
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_isboolean( p_lua : pointer; p_index : integer ) : boolean;

    (*
     * check if the element is a thread
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_isthread( p_lua : pointer; p_index : integer ) : boolean;

    (*
     * check if the element is none
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_isnone( p_lua : pointer; p_index : integer ) : boolean;

    (*
     * check if the element is a non or nil
     * @param p_lua lua engine pointer
     * @param p_index stack index
     * @return boolean check result
     *)
     function lua_isnoneornil( p_lua : pointer; p_index : integer ) : boolean;

    (*
    * pushs a c-function to the stack
    * @param p_lua lua engine pointer
    * @param p_ptr c-function pointer
    *)
    procedure lua_pushcfunction( p_lua : pointer; p_ptr : pointer );

     (*
      * register a function
      * @param p_lua lua engine pointer
      * @param p_name function name
      * @param p_ptr function pointer
      *)
     procedure lua_register( p_lua : pointer; p_name : pchar; p_ptr : pointer );


implementation

    procedure lua_pop( p_lua : pointer; p_index : integer );
    begin
        lua_settop( p_lua, -(p_index)-1 )
    end;

    procedure lua_remove( p_lua : pointer; p_index : integer );
    begin
        lua_rotate( p_lua, p_index, -1 );
        lua_pop( p_lua, 1 );
    end;

    function lua_tostring( p_lua : pointer; p_index : integer ) : pchar;
    begin
        result := lua_tolstring( p_lua, p_index, 0 );
    end;

    function lua_tonumber( p_lua : pointer; p_index : integer ) : double;
    begin
        result := lua_tonumberx( p_lua, p_index, nil );
    end;

    function lua_tointeger( p_lua : pointer; p_index : integer ) : integer;
    begin
        result := lua_tointegerx( p_lua, p_index, nil );
    end;

    function lua_pcall( p_lua : pointer; p_args : integer; p_results : integer; p_errorfunction : integer ) : integer;
    begin
        result := lua_pcallk( p_lua, p_args, p_results, p_errorfunction, nil, nil );
    end;

    procedure lua_call( p_lua : pointer; p_args : integer; p_results : integer );
    begin
        lua_callk( p_lua, p_args, p_results, nil, nil  );
    end;

    function lua_isfunction( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) = LUA_TFUNCTION;
    end;

    function lua_istable( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) = LUA_TTABLE;
    end;

    function lua_islightuserdata( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) = LUA_TLIGHTUSERDATA;
    end;

    function lua_isnil( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) = LUA_TNIL;
    end;

    function lua_isboolean( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) = LUA_TBOOLEAN;
    end;

    function lua_isthread( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) = LUA_TTHREAD;
    end;

    function lua_isnone( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) = LUA_TNONE;
    end;

    function lua_isnoneornil( p_lua : pointer; p_index : integer ) : boolean;
    begin
        result := lua_type( p_lua, p_index ) <= 0;
    end;

    procedure lua_pushcfunction( p_lua : pointer; p_ptr : pointer );
    begin
        lua_pushcclosure( p_lua, p_ptr, 0 );
    end;

    procedure lua_register( p_lua : pointer; p_name : pchar; p_ptr : pointer );
    begin
        lua_pushcfunction( p_lua, p_ptr );
        lua_setglobal( p_lua, p_name );
    end;

end.
