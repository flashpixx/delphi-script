# Delphi-Script

* [Lua](https://www.lua.org/)

## Lua

Lua static libraries can be build with [Scons](https://scons.org/)

### Mac OS X

1. Install XCode Developling tools (Homebrew need the tools)
2. Install [Homebrew](https://brew.sh) 
3. Install Python with `brew install python`
4. Install Scons with `pip install scons`
5. extract the downloaded Lua source codes within `lib/lua/lua/`
6. run `scons` from the command-line within the `lib/lua/` directory

### Windows - Visual Studio Code

1. Install [Visual Studio](https://visualstudio.microsoft.com/de/downloads/) (Community Edition is enough) with Python and C/C++ Desktop Application support
2. Install Scons with `py -m pip install scons` from command-line
3. Add path of the `scons.bat` to the environment path variable
4. extract the downloaded Lua source codes within `lib/lua/lua/`
5. Open `x86` or `x64` native command-line
6. run `scons` from the command-line within the `lib/lua/` directory
