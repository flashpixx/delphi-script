(*
 * ######################################################################################
 * # LGPL License                                                                       #
 * #                                                                                    #
 * # This file is part of the Delphi-Script                                             #
 * # Copyright (c) 2019, Delphi-Script (philipp.kraus@flashpixx.de)                     #
 * # This program is free software: you can redistribute it and/or modify               #
 * # it under the terms of the GNU Lesser General Public License as                     #
 * # published by the Free Software Foundation, either version 3 of the                 #
 * # License, or (at your option) any later version.                                    #
 * #                                                                                    #
 * # This program is distributed in the hope that it will be useful,                    #
 * # but WITHOUT ANY WARRANTY; without even the implied warranty of                     #
 * # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                      #
 * # GNU Lesser General Public License for more details.                                #
 * #                                                                                    #
 * # You should have received a copy of the GNU Lesser General Public License           #
 * # along with this program. If not, see http://www.gnu.org/licenses/                  #
 * ######################################################################################
 *)

uses sysutils, LuaEngine;

(*
 * print arguments on the command-line with types
 * @param p_argument argument object
 *)
procedure printarguments( const p_argument : Argument );
var
    i : longint;
begin
    if p_argument.IsEmpty() then
        Writeln( 'no arguments' )
    else
        for i := 0 to p_argument.GetSize() - 1 do
        begin
            if p_argument.IsNull( i ) then
                Write('<empty>')
            else
                Write( p_argument.GetValue( i ) );
            Write( char(9) + char(9) );

            if p_argument.IsString( i ) then
                Write('- string ');
            if p_argument.IsInteger( i ) then
                Write('- integer ');
            if p_argument.IsDouble( i ) then
                Write('- double ');
            if p_argument.IsBoolean( i ) then
                Write('- boolean ');
            if p_argument.IsNull( i ) then
                Write('- null ');

            Writeln();
        end;
end;

(*
 * pascal function for calling from lua, must be a c-function (cdecl)
 * @param p_lua pointer to lua engine
 * @return number of arguments, which must be pushed to the stack first
 *)
function pascalfunction( p_lua : pointer ) : integer; cdecl;
begin
    // runs any pascal code
    Writeln('calling pascal function');

    // parses given arguments (and write it)
    printarguments( Lua.StackToArgument( p_lua ) );

    // push the result arguments of the function to the stack and ...
    Lua.Push( p_lua, 'function result' );
    // ... returns the number of arguments back
    result := 1;
end;



var
    l_engine : Lua;
begin

    // --- initialize engine with a lua script ---

    l_engine := Lua.Create(
        'print("--- lua script ---")' + char(10) +

        'function foo()' + char(10) +
            'print("bar")' + char(10) +
        'end' + char(10) +

        'function params( a, b, c, d, e )' + char(10) +
            'print("parameter", a, b, c, d, e)' + char(10) +
        'end' + char(10) +

        'function withreturn()' + char(10) +
            'return "abcd", 123, 5.34, false, nil' + char(10) +
        'end'
    );



    // --- get engine version ---
    Writeln( l_engine.GetVersion() );

    Writeln();


    // --- check functions exists ---

    Writeln( format( 'function [%s] exists: %s', ['foobar', booltostr( l_engine.IsExecutable( 'foobar' ) )] ) );
    Writeln( format( 'function [%s] exists: %s', ['foo', booltostr( l_engine.IsExecutable( 'foo' ) )] ) );

    Writeln();

    // --- execution lua function also with arguments ---

    l_engine.Execute( 'foo' );
    l_engine.Execute( 'params', ['foo', 1, 2.34, true] );

    Writeln();

    // --- execution lua function with return ---

    printarguments( l_engine.Execute( 'withreturn' ) );

    Writeln();


    // --- global variables ---

    Writeln(
        Lua.Create()
        .SetGlobal( 'MYNUM', 123 )
        .SetGlobal( 'MYSTR', 'very long var with text' )
        .parse(
            'print("--- lua script globals ---")' + char(10) +
            'print(MYNUM)' + char(10) +
            'print(MYSTR)' + char(10) +
            'OUT = "output string"'
        )
        .GetGlobal( 'OUT' )
        .GetValue( 0 )
    );

    Writeln();


    // --- call pascal function from lua script and print result values ---

    Lua.Create()
    .AddFunction( 'myfunction', pascalfunction )
    .parse(
        'print("--- lua script external ---")' + char(10) +

        'x = myfunction()' + char(10) +
        'print("Function Result: ", x)' + char(10) +

        'print()' + char(10) +

        'y = myfunction( "a long text", 123, 5.78, true, nil )' + char(10) +
        'print("Function Result: ",y)'
    );

end.
